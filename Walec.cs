﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Figury_Zadanie_Domowe
{
    class Walec : Figura
    {
        private double h { get; set; }
        private double r { get; set; }

        public Walec() { }

        public override void wpisz()
        {
            Console.WriteLine("promien podstawy walca: ");
            r = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("wysokosc walca: ");
            h = Convert.ToDouble(Console.ReadLine());
        }

        public override double licz_objetosc()
        {
            return Math.Round(((Math.PI * Math.Pow(r, 2) * h)),2);
        }

        public override double licz_pole()
        {
            return Math.Round(((2*Math.PI * Math.Pow(r, 2)) + (2 * Math.PI * r * h)),2);
        }

        public override string zwroc_nazwe()
        {
            return ("Walec");
        }
    }
}
