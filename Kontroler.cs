﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Figury_Zadanie_Domowe
{
    class Kontroler
    {
        private List<Figura> zbior = new List<Figura>();

        public void uzupelnij_figury()
        {
            zbior.Add(new Kula());
            zbior.Add(new Ostroslup());
            zbior.Add(new Prostopadloscian());
            zbior.Add(new Stozek());
            zbior.Add(new Walec());
        }

        public void menu_wyboru()
        {
            bool wybor = true;
            string co_liczyc, decyzja;
            int x = 1;

            while(wybor==true)
            {
                Console.WriteLine("Menu: \n");

                foreach (Figura figura in zbior)
                {
                    Console.WriteLine("{0}. {1}",x,figura.zwroc_nazwe());
                    x++;
                }

                Console.WriteLine("{0}. Exit",x);


                x = 1;

                Console.WriteLine("\nPodaj figure: ");
                co_liczyc = Console.ReadLine().ToLower();
                Console.Clear();

                switch(co_liczyc)
                {
                    case "kula":
                        zbior[0].wpisz();
                        Console.WriteLine("Pole kuli: \n{0}",zbior[0].licz_pole());
                        Console.WriteLine("Objetosc kuli: \n{0}", zbior[0].licz_objetosc());
                        break;

                    case "ostroslup":
                        zbior[1].wpisz();
                        Console.WriteLine("Pole ostroslupa: \n{0}", zbior[1].licz_pole());
                        Console.WriteLine("Objetosc ostroslupa: \n{0}", zbior[1].licz_objetosc());
                        break;

                    case "prostopadloscian":
                        zbior[2].wpisz();
                        Console.WriteLine("Pole prostopadloscianu: \n{0}", zbior[2].licz_pole());
                        Console.WriteLine("Objetosc prostopadloscianu: \n{0}", zbior[2].licz_objetosc());
                        break;

                    case "stozek":
                        zbior[3].wpisz();
                        Console.WriteLine("Pole stozka: \n{0}", zbior[3].licz_pole());
                        Console.WriteLine("Objetosc stozka: \n{0}", zbior[3].licz_objetosc());
                        break;

                    case "walec":
                        zbior[4].wpisz();
                        Console.WriteLine("Pole walca: \n{0}", zbior[4].licz_pole());
                        Console.WriteLine("Objetosc stozka: \n{0}", zbior[4].licz_objetosc());
                        break;

                    case "exit":
                        System.Environment.Exit(1);
                        break;

                    default:
                        break;
                }

                Console.WriteLine("\nCzy chcesz jeszcze liczyc? (tak/nie)");
                decyzja = Console.ReadLine().ToLower();
                Console.Clear();

                if (decyzja=="nie")
                {
                    wybor = false;
                }
            }
        }
    }
}
