﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Figury_Zadanie_Domowe
{
    class Kula : Figura
    {
        private double r { get; set; }

        public Kula() { }

        public override void wpisz()
        {
            Console.WriteLine("promien kuli: ");
            r = Convert.ToDouble(Console.ReadLine());
        }

        public override double licz_objetosc()
        {
            return Math.Round(((4 * Math.PI * Math.Pow(r, 3))/3),2);
        }

        public override double licz_pole()
        {
            return Math.Round((4*Math.PI*Math.Pow(r,2)),2);
        }

        public override string zwroc_nazwe()
        {
            return ("Kula");
        }
    }
}
