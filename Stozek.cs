﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Figury_Zadanie_Domowe
{
    class Stozek : Figura
    {
        private double h { get; set; }
        private double r { get; set; }
        private double l { get; set; }

        public Stozek() { }

        public override void wpisz()
        {
            Console.WriteLine("promien podstawy prostopadloscianu: ");
            r = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("wysokosc prostopadloscianu: ");
            h = Convert.ToDouble(Console.ReadLine());
            this.l = Math.Sqrt(Math.Pow(h, 2) + Math.Pow(r, 2));
        }

        public override double licz_objetosc()
        {
            return Math.Round(((Math.PI * Math.Pow(r, 2) * h) / 3),2);
        }

        public override double licz_pole()
        {
            return Math.Round((Math.PI * Math.Pow(r, 2) + Math.PI * r * l),2);
        }

        public override string zwroc_nazwe()
        {
            return ("Stozek");
        }
    }
}
