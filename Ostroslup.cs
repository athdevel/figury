﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Figury_Zadanie_Domowe
{
    class Ostroslup : Figura
    {
        private double a { get; set; }
        private double b { get; set; }
        private double h { get; set; }

        public Ostroslup() { }
    
        public override void wpisz()
        {
            Console.WriteLine("a podstawy ostroslupa: ");
            a = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("b podstawy ostroslupa: ");
            b = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("wysokosc ostroslupa: ");
            h = Convert.ToDouble(Console.ReadLine());
        }
   
        public override double licz_objetosc()
        {
            return Math.Round(((a * b * h)/3),2);
        }

        public override double licz_pole()
        {
            return Math.Round(((a*b) + (a*h) + (b*h)),2);
        }

        public override string zwroc_nazwe()
        {
            return ("Ostroslup");
        }
    }
}
