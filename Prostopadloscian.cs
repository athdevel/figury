﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Figury_Zadanie_Domowe
{
    class Prostopadloscian : Figura
    {
        private double a { get; set; }
        private double b { get; set; }
        private double h { get; set; }

        public Prostopadloscian() { }

        public override void wpisz()
        {
            Console.WriteLine("a podstawy prostopadloscianu: ");
            a = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("b podstawy prostopadloscianu: ");
            b = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("wysokosc prostopadloscianu: ");
            h = Convert.ToDouble(Console.ReadLine());
        }

        public override double licz_objetosc()
        {
            return Math.Round((a * b * h),2);
        }

        public override double licz_pole()
        {
            return Math.Round(((2 * a * b) + (2*b*h) + (2*a*h)),2);
        }

        public override string zwroc_nazwe()
        {
            return ("Prostopadloscian");
        }
    }
}
