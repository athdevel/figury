﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Figury_Zadanie_Domowe
{
    // #polimorfizm
    abstract class Figura
    {
        public abstract double licz_pole();
        public abstract double licz_objetosc();
        public abstract string zwroc_nazwe();
        public abstract void wpisz();
    }
}
